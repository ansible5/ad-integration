Keystone - AD integration
=========

Scope of this role is to deploy a pair of haproxy containers on the hosts having external connectivity that can be used to forward LDAPS traffic from keystone pods to AD servers.

Requirements
------------

Following extra-vars need to be set:

- `ldap_keystone_domain_conf` needs to be set as below example:

More info in http://docs.oiaas-pages.itn/Keystone-LDAP-integration/#setting-up-active-directory-as-ldap-backend

  ```yaml
  ldap_keystone_domain_conf:
    url: "ldaps://active-directory.ad.oiaas:636"
    user: "cn=LDAP Lookups,cn=Users,dc=ad,dc=oiaas"
    password: "lookup-user-password"
    user_tree_dn: "cn=Users,dc=ad,dc=oiaas"
    user_filter: "(memberOf=cn=grp-openstack,cn=Users,dc=ad,dc=oiaas)"
    user_objectclass: "user"
    user_id_attribute: "sAMAccountName"
    user_name_attribute: "sAMAccountName"
    user_mail_attribute: "mail"
    user_enabled_attribute: "userAccountControl"
    user_enabled_mask: 2
    user_enabled_default: 512
    user_attribute_ignore: "password,tenant_id,tenants"
    group_objectclass: "group"
    group_tree_dn: "cn=Users,dc=ad,dc=oiaas"
    group_filter: "(cn=grp-openstack*)"
    group_id_attribute: "cn"
    group_name_attribute: "name"
    tls_cacertfile: "/etc/ssl/certs/ca-adds.pem"
    tls_req_cert: "allow"
    query_scope: "sub"
    chase_referrals: false
  ```

- `ldap_ca_cert ` (certificates supplied by the country: **Root CA** + **Subordinate CA**) must be set as below example:

  ```yaml
  ldap_ca_cert: |
    -----BEGIN CERTIFICATE-----
    ...
    -----END CERTIFICATE-----
    -----BEGIN CERTIFICATE-----
    ...
    -----END CERTIFICATE-----
  ```

Dependencies
------------

none

Example Playbook
----------------

```yaml
---
- hosts: dns
  roles:
   - role: ad-integration
```
